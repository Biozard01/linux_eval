#!/bin/bash

if [[ ! $(ls /tmp/in/) ]]; then
    mkdir /tmp/out
    mkdir /tmp/in
    echo "WARNING : /tmp/in absent ou vide, veuillez ajoutez un ou plusieurs fichier dans le dossier /tmp/in, puis re-exécuter la commande"
    
else
    if test $(ls /tmp/out/lock); then 
        exit 22
    fi

    touch /tmp/out/lock

    for i in $(ls /tmp/in)
        do     
            if [[ ${i##*.} != "gz" ]]; then
                gzip /tmp/in/$i
				if [ $? != 0 ]; then 
                    echo "Fichier "$i" non compressé " >> /tmp/out/log
				else
					mv /tmp/in/"$i.gz" /tmp/out/
					echo "Fichier "$i" est compressé" >> /tmp/out/log
		            
				fi
            else
                if $i != "$i.gz"; then
            	    echo "Fichier "$i" non compressé " >> /tmp/out/log
                fi
            fi

        done
    
fi

rm -rf /tmp/out/lock
